﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using TvMazeScraperTest.Common.Mapper;
using TvMazeScraperTest.Core.Models;
using TvMazeScraperTest.Repositories.Entities;
using TvMazeScraperTest.Repositories.Enums;
using TvMazeScraperTest.Common;

namespace TvMazeScraperTest.Core.Infrastructure
{
	public static class MappingRegistrar
	{
		internal static void Register()
		{
			Mapper.CreateMap<ShowEntity, ShowWithActorsModel>()
				.ConvertUsing( show => new ShowWithActorsModel()
				{
					Id = show.Id,
					Seasons = show.Seasons,
					Started = show.Started,
					Name = show.Name,
					Actors = show.ShowParticipants.Where(p => p.ParticipantEntity.Role == ParticipantRole.Actor)
						.Select(t => new ActorModel()
						{
							BirthDate = t.ParticipantEntity.BirthDate,
							Id = t.ParticipantId,
							PatronymicName = t.ParticipantEntity.PatronymicName,
							FirstName = t.ParticipantEntity.FirstName,
							LastName = t.ParticipantEntity.LastName
						})
						.ToList(),
					Genres = Mapper.Map<List<GenreModel>>(show.Genres),
					Shedules = Mapper.Map<List<SheduleModel>>(show.Shedules)
				});

			Mapper.CreateMap<GenreEntity, GenreModel>()
				.ConvertUsing( genre => new GenreModel()
				{
					Genre = genre.ShowGenre.GetDescription(),
					Id = genre.Id
				});

			Mapper.CreateMap<SheduleEntity, SheduleModel>()
				.ConvertUsing(shedule => new SheduleModel()
				{
					Id = shedule.Id,
					Time = shedule.Time.ToString("g"),
					DayOfWeek = DateTimeFormatInfo.CurrentInfo.GetDayName(shedule.DayOfWeek)
		});
		}
	}
}