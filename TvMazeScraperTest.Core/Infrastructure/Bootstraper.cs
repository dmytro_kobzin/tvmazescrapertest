﻿using Microsoft.Extensions.DependencyInjection;
using TvMazeScraperTest.Core.Infrastructure;
using TvMazeScraperTest.Core.Services;
using TvMazeScraperTest.Repositories.Repositories;

namespace TvMazeScraperTest.Core
{
	/// <summary>
	/// Bootstrapper
	/// </summary>
	public static class Bootstraper
	{
		/// <summary>
		/// Registers this instance.
		/// </summary>
		/// <param name="services">The services.</param>
		public static void Register(IServiceCollection services)
		{
			services.AddTransient<IShowService, ShowService>();
		}

		/// <summary>
		/// Registers the mappings.
		/// </summary>
		public static void RegisterMappings()
		{
			MappingRegistrar.Register();
		}
	}
}