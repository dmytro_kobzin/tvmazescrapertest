﻿using System.Collections.Generic;
using TvMazeScraperTest.Core.Models;

namespace TvMazeScraperTest.Core.Services
{
	/// <summary>
	/// Interface of showEntity service
	/// </summary>
	/// <seealso cref="TvMazeScraperTest.Core.Services.IService" />
	public interface IShowService : IService
	{
		/// <summary>
		/// Gets the showEntity with actors.
		/// </summary>
		/// <param name="showId">The showEntity identifier.</param>
		/// <returns></returns>
		ShowWithActorsModel GetShowWithActors(int showId);

		/// <summary>
		/// Searches the specified name.
		/// </summary>
		/// <param name="query">The query.</param>
		/// <returns></returns>
		ICollection<ShowWithActorsModel> Search(string query);
	}
}