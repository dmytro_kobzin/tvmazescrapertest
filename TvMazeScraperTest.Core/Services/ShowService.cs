﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using TvMazeScraperTest.Core.Models;
using TvMazeScraperTest.Repositories;
using TvMazeScraperTest.Repositories.Entities;
using TvMazeScraperTest.Repositories.Enums;
using TvMazeScraperTest.Repositories.Repositories;
using TvMazeScraperTest.Common.Mapper;

namespace TvMazeScraperTest.Core.Services
{
	/// <summary>
	/// ShowEntity service.
	/// </summary>
	/// <seealso cref="TvMazeScraperTest.Core.Services.IShowService" />
	public class ShowService : IShowService
    {
		/// <summary>
		/// The showEntity repository.
		/// </summary>
		protected readonly IShowRepository ShowRepository;

	    public ShowService(IShowRepository repository)
	    {
		    this.ShowRepository = repository;
	    }

	    public virtual ShowWithActorsModel GetShowWithActors(int showId)
	    {
		    var show = this.ShowRepository.Get(showId);
			var showModel = Mapper.Map<ShowWithActorsModel>(show);

		    return showModel;
	    }

		/// <summary>
		/// Searches the specified query.
		/// </summary>
		/// <param name="query">The query.</param>
		/// <returns></returns>
		public ICollection<ShowWithActorsModel> Search(string query)
	    {
			// TO-DO
		    var shows = this.ShowRepository.Search(show => show.Name.Contains(query));
		    var models = Mapper.Map<ICollection<ShowWithActorsModel>>(shows);

		    return models;
	    }
    }
}
