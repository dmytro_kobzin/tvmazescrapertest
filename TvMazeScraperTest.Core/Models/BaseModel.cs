﻿namespace TvMazeScraperTest.Core.Models
{
	/// <summary>
	/// Base model.
	/// </summary>
	public class BaseModel
	{
		/// <summary>
		/// Gets or sets the identifier.
		/// </summary>
		/// <value>
		/// The identifier.
		/// </value>
		public virtual int Id { get; set; }
	}
}