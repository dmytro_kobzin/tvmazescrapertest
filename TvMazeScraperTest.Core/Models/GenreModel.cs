﻿using TvMazeScraperTest.Repositories.Enums;

namespace TvMazeScraperTest.Core.Models
{
	/// <summary>
	/// Genre model
	/// </summary>
	/// <seealso cref="TvMazeScraperTest.Core.Models.BaseModel" />
	public class GenreModel : BaseModel
	{
		/// <summary>
		/// Gets or sets the genre.
		/// </summary>
		/// <value>
		/// The genre.
		/// </value>
		public string Genre { get; set; }
	}
}