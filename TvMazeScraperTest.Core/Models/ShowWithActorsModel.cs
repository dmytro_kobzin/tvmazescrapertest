﻿using System;
using System.Collections.Generic;
using TvMazeScraperTest.Repositories.Entities;

namespace TvMazeScraperTest.Core.Models
{
	/// <summary>
	/// ShowEntity model
	/// </summary>
	/// <seealso cref="TvMazeScraperTest.Core.Models.BaseModel" />
	public class ShowWithActorsModel : BaseModel
	{
		public ShowWithActorsModel()
		{
			this.Actors = new List<ActorModel>();
			this.Genres = new List<GenreModel>();
			this.Shedules = new List<SheduleModel>();
		}

		public virtual string Name { get; set; }

		/// <summary>
		/// Gets or sets the started.
		/// </summary>
		/// <value>
		/// The started.
		/// </value>
		public virtual DateTime Started { get; set; }

		/// <summary>
		/// Gets or sets the finished.
		/// </summary>
		/// <value>
		/// The finished.
		/// </value>
		public virtual DateTime? Finished { get; set; }

		/// <summary>
		/// Gets or sets the seasons.
		/// </summary>
		/// <value>
		/// The seasons.
		/// </value>
		public virtual int Seasons { get; set; }

		/// <summary>
		/// Gets or sets the actors.
		/// </summary>
		/// <value>
		/// The actors.
		/// </value>
		public List<ActorModel> Actors { get; set; }

		/// <summary>
		/// Gets or sets the genres.
		/// </summary>
		/// <value>
		/// The genres.
		/// </value>
		public List<GenreModel> Genres { get; set; }

		/// <summary>
		/// Gets or sets the shedules.
		/// </summary>
		/// <value>
		/// The shedules.
		/// </value>
		public List<SheduleModel> Shedules { get; set; }
	}
}