﻿using System;

namespace TvMazeScraperTest.Core.Models
{
	/// <summary>
	/// Shedule model.
	/// </summary>
	/// <seealso cref="TvMazeScraperTest.Core.Models.BaseModel" />
	public class SheduleModel : BaseModel
	{
		/// <summary>
		/// Gets or sets the time.
		/// </summary>
		/// <value>
		/// The time.
		/// </value>
		public virtual string Time { get; set; }

		/// <summary>
		/// Gets or sets the day of week.
		/// </summary>
		/// <value>
		/// The day of week.
		/// </value>
		public virtual string DayOfWeek { get; set; }
	}
}