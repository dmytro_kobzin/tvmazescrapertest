﻿namespace TvMazeScraperTest.Core.Models
{
	/// <summary>
	/// Interfase Base model
	/// </summary>
	public interface IBaseModel
	{
		/// <summary>
		/// Gets or sets the identifier.
		/// </summary>
		/// <value>
		/// The identifier.
		/// </value>
		int Id { get; set; }
	}
}