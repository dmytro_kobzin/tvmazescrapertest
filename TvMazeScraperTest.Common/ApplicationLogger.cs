﻿using System;
using NLog;

namespace TvMazeScraperTest.Common
{
	/// <summary>
	/// Application logger wrapper.
	/// </summary>
	public static class ApplicationLogger
	{
		/// <summary>
		/// Gets the logger.
		/// </summary>
		/// <value>
		/// The logger.
		/// </value>
		public static Logger Logger
		{
			get
			{
				NLog.LogManager.Configuration = new NLog.Config.XmlLoggingConfiguration(Environment.CurrentDirectory + "\\NLog.config", true);
				return LogManager.GetCurrentClassLogger();
			}
		}
	}
}