﻿using System;
using AutoMapper;
using AutoMapper.Configuration;

namespace TvMazeScraperTest.Common.Mapper
{
	/// <summary>
	/// Mapper
	/// </summary>
	public static class Mapper
	{
		private const string InvalidOperationMessage = "Mapper not initialized. Call Initialize.";
		private static readonly MapperConfigurationExpression Configuration = new MapperConfigurationExpression();
		private static MapperConfiguration mapperConfig;
		private static IMapper _mapper;

		/// <summary>
		/// Gets or sets the instance.
		/// </summary>
		/// <value>
		/// The instance.
		/// </value>
		/// <exception cref="System.InvalidOperationException"></exception>
		private static IMapper Instance
		{
			get
			{
				if (_mapper == null)
				{
					throw new InvalidOperationException(InvalidOperationMessage);
				}

				return _mapper;
			}

			set
			{
				_mapper = value;
			}
		}

		/// <summary>
		/// Creates the map.
		/// </summary>
		/// <typeparam name="TSource">The type of the source.</typeparam>
		/// <typeparam name="TDestination">The type of the destination.</typeparam>
		/// <returns></returns>
		public static IMappingExpression<TSource, TDestination> CreateMap<TSource, TDestination>()
		{
			return Configuration.CreateMap<TSource, TDestination>();
		}

		/// <summary>
		/// Adds the profile.
		/// </summary>
		/// <param name="type">The type.</param>
		public static void AddProfile(Type type)
		{
			Configuration.AddProfile(type);
		}

		/// <summary>
		/// Maps the specified source.
		/// </summary>
		/// <typeparam name="TSource">The type of the source.</typeparam>
		/// <typeparam name="TDestination">The type of the destination.</typeparam>
		/// <param name="source">The source.</param>
		/// <returns></returns>
		public static TDestination Map<TSource, TDestination>(TSource source)
		{
			return Instance.Map<TSource, TDestination>(source);
		}

		/// <summary>
		/// Maps the specified source.
		/// </summary>
		/// <typeparam name="TDestination">The type of the destination.</typeparam>
		/// <param name="source">The source.</param>
		/// <returns></returns>
		public static TDestination Map<TDestination>(object source)
		{
			return Instance.Map<TDestination>(source);
		}

		/// <summary>
		/// Maps the specified source.
		/// </summary>
		/// <typeparam name="TSource">The type of the source.</typeparam>
		/// <typeparam name="TDestination">The type of the destination.</typeparam>
		/// <param name="source">The source.</param>
		/// <param name="destination">The destination.</param>
		/// <returns></returns>
		public static TDestination Map<TSource, TDestination>(TSource source, TDestination destination)
		{
			return Instance.Map(source, destination);
		}

		/// <summary>
		/// Initializes this instance.
		/// </summary>
		public static void Initialize()
		{
			mapperConfig = new MapperConfiguration(Configuration);
			Instance = mapperConfig.CreateMapper();
		}

		/// <summary>
		/// Asserts the configuration is valid.
		/// </summary>
		public static void AssertConfigurationIsValid()
		{
			Instance.ConfigurationProvider.AssertConfigurationIsValid();
		}

		/// <summary>
		/// Asserts the configuration is valid.
		/// </summary>
		/// <param name="profileName">Name of the profile.</param>
		public static void AssertConfigurationIsValid(string profileName)
		{
			Instance.ConfigurationProvider.AssertConfigurationIsValid(profileName);
		}
	}
}