﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TvMazeScraperTest.Api.ViewModel;
using TvMazeScraperTest.Core.Models;
using TvMazeScraperTest.Core.Services;
using TvMazeScraperTest.Repositories;
using TvMazeScraperTest.Common.Mapper;

namespace TvMazeScraperTest.Api.Controllers
{
	/// <summary>
	/// ShowEntity controller
	/// </summary>
	/// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
	[Produces("application/json")]
    [Route("api/ShowEntity")]
    public class ShowController : Controller
    {
	    private readonly IShowService ShowService;

	    public ShowController(IShowService service)
	    {
			this.ShowService = service;
	    }

	    // GET: api/ShowEntity
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

		// GET: api/ShowEntity/5
		/// <summary>
		/// Gets the specified identifier.
		/// </summary>
		/// <param name="id">The identifier.</param>
		/// <returns></returns>
		[HttpGet("{id}", Name = "Get")]
        public ShowWithActorsViewModel Get(int id)
        {
			var model =this.ShowService.GetShowWithActors(id);
			var viewModel = Mapper.Map<ShowWithActorsViewModel>(model);
	        return viewModel;
        }

		[HttpGet("search/{query}", Name = "Search")]
		public ICollection<ShowWithActorsViewModel> Search(string query)
		{
			var model = this.ShowService.Search(query);
			var viewModel = Mapper.Map<List<ShowWithActorsViewModel>>(model);
			return viewModel;
		}

		// POST: api/ShowEntity
		[HttpPost]
        public void Post([FromBody]string value)
        {
        }
        
        // PUT: api/ShowEntity/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
