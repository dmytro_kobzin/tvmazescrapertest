﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using TvMazeScraperTest.Api.Infrastructure.Mapper;
using TvMazeScraperTest.Common;
using TvMazeScraperTest.Repositories;
using TvMazeScraperTest.Core;

namespace TvMazeScraperTest.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
	        services.Configure<CookiePolicyOptions>(options =>
	        {
		        // This lambda determines whether user consent for non-essential cookies is needed for a given request.
		        options.MinimumSameSitePolicy = SameSiteMode.None;
	        });
			
	        services.AddMvc();

			ConfigureSwaggerServices(services);

			Core.Bootstraper.Register(services);

			Repositories.Bootstraper.Register(services);

	        Core.Bootstraper.RegisterMappings();

	        Repositories.Bootstraper.RegisterMappings();

			MappingRegistrar.Register();

	        MappingRegistrar.Initialize();
			
		}

	    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
	        if (env.IsDevelopment())
	        {
		        AppConfigureSwagger(app);

		        app.UseDeveloperExceptionPage();
	        }

	        app.UseStaticFiles();
	        app.UseCookiePolicy();

			app.UseMvc();
        }

		/// <summary>
		/// Applications the configure swagger.
		/// </summary>
		/// <param name="app">The application.</param>
		private static void AppConfigureSwagger(IApplicationBuilder app)
	    {
		    app.UseSwagger();

		    app.UseSwaggerUI(c =>
		    {
			    c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
			    c.RoutePrefix = string.Empty;
		    });
	    }

		/// <summary>
		/// Configures the swagger services.
		/// </summary>
		/// <param name="services">The services.</param>
		private static void ConfigureSwaggerServices(IServiceCollection services)
	    {
		    services.AddSwaggerGen(c => { c.SwaggerDoc("v1", new Info() { Title = "Azure Web Api Test", Version = "v1" }); });
	    }
	}
}
