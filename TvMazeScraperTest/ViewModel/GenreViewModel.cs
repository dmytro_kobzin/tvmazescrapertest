﻿namespace TvMazeScraperTest.Api.ViewModel
{
	/// <summary>
	/// Genre viewmodel
	/// </summary>
	/// <seealso cref="TvMazeScraperTest.Api.ViewModel.BaseViewModel" />
	public class GenreViewModel : BaseViewModel
	{
		/// <summary>
		/// Gets or sets the genre.
		/// </summary>
		/// <value>
		/// The genre.
		/// </value>
		public virtual string Genre { get; set; }
	}
}