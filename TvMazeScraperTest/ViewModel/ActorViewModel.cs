﻿using System;

namespace TvMazeScraperTest.Api.ViewModel
{
	public class ActorViewModel : BaseViewModel
	{
		/// <summary>
		/// Gets or sets the first name.
		/// </summary>
		/// <value>
		/// The first name.
		/// </value>
		public virtual string FirstName { get; set; }

		/// <summary>
		/// Gets or sets the name of the patronymic.
		/// </summary>
		/// <value>
		/// The name of the patronymic.
		/// </value>
		public virtual string PatronymicName { get; set; }

		/// <summary>
		/// Gets or sets the last name.
		/// </summary>
		/// <value>
		/// The last name.
		/// </value>
		public virtual string LastName { get; set; }

		/// <summary>
		/// Gets or sets the birth date.
		/// </summary>
		/// <value>
		/// The birth date.
		/// </value>
		public virtual DateTime BirthDate { get; set; }
	}
}