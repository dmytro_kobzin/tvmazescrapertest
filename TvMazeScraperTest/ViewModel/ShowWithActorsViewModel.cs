﻿using System;
using System.Collections.Generic;

namespace TvMazeScraperTest.Api.ViewModel
{
	/// <summary>
	/// ShowEntity with actors model.
	/// </summary>
	/// <seealso cref="TvMazeScraperTest.Api.ViewModel.BaseViewModel" />
	public class ShowWithActorsViewModel : BaseViewModel
	{
		public ShowWithActorsViewModel()
		{
			this.Actors = new List<ActorViewModel>();
			this.Genres = new List<GenreViewModel>();
			this.Shedules = new List<SheduleViewModel>();
		}

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		public virtual string Name { get; set; }

		/// <summary>
		/// Gets or sets the started.
		/// </summary>
		/// <value>
		/// The started.
		/// </value>
		public virtual DateTime Started { get; set; }

		/// <summary>
		/// Gets or sets the finished.
		/// </summary>
		/// <value>
		/// The finished.
		/// </value>
		public virtual DateTime? Finished { get; set; }

		/// <summary>
		/// Gets or sets the seasons.
		/// </summary>
		/// <value>
		/// The seasons.
		/// </value>
		public virtual int Seasons { get; set; }

		/// <summary>
		/// Gets or sets the actors.
		/// </summary>
		/// <value>
		/// The actors.
		/// </value>
		public virtual ICollection<ActorViewModel> Actors { get; set; }

		/// <summary>
		/// Gets or sets the genres.
		/// </summary>
		/// <value>
		/// The genres.
		/// </value>
		public List<GenreViewModel> Genres { get; set; }

		/// <summary>
		/// Gets or sets the shedules.
		/// </summary>
		/// <value>
		/// The shedules.
		/// </value>
		public List<SheduleViewModel> Shedules { get; set; }
	}
}