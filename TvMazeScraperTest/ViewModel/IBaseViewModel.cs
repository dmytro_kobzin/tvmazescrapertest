﻿namespace TvMazeScraperTest.Api.ViewModel
{
	public interface IBaseViewModel
	{
		/// <summary>
		/// Gets or sets the identifier.
		/// </summary>
		/// <value>
		/// The identifier.
		/// </value>
		int Id { get; set; }
	}
}