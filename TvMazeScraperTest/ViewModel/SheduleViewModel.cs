﻿namespace TvMazeScraperTest.Api.ViewModel
{
	/// <summary>
	/// Shedule view model
	/// </summary>
	/// <seealso cref="TvMazeScraperTest.Api.ViewModel.BaseViewModel" />
	public class SheduleViewModel : BaseViewModel
	{
		/// <summary>
		/// Gets or sets the time.
		/// </summary>
		/// <value>
		/// The time.
		/// </value>
		public virtual string Time { get; set; }

		/// <summary>
		/// Gets or sets the day of week.
		/// </summary>
		/// <value>
		/// The day of week.
		/// </value>
		public virtual string DayOfWeek { get; set; }
	}
}