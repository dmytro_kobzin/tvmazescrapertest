﻿using TvMazeScraperTest.Common.Mapper;

namespace TvMazeScraperTest.Api.Infrastructure.Mapper
{
	public static class MappingRegistrar
	{
		internal static void Register()
		{
			Common.Mapper.Mapper.AddProfile(typeof(AutoMapperProfile));
		}

		public static void Initialize()
		{
			Common.Mapper.Mapper.Initialize();
		}
	}
}