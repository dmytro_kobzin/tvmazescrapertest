﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using TvMazeScraperTest.Api.ViewModel;
using TvMazeScraperTest.Core.Models;

namespace TvMazeScraperTest.Api.Infrastructure.Mapper
{
	public class AutoMapperProfile : Profile
	{
		public AutoMapperProfile()
		{
			this.RegisterMappingViewModels();
		}

		private void RegisterMappingViewModels()
		{
			this.CreateMap<ActorModel, ActorViewModel>();
			this.CreateMap<GenreModel, GenreViewModel>();
			this.CreateMap<SheduleModel, SheduleViewModel>();

			this.CreateMap<ShowWithActorsModel, ShowWithActorsViewModel>()
				.ConstructUsing(model => new ShowWithActorsViewModel()
				{
					Id = model.Id,
					Name = model.Name,
					Seasons = model.Seasons,
					Started = model.Started,
					Finished = model.Finished,
					Actors = model.Actors.Select(t => new ActorViewModel()
					{
						Id = t.Id,
						FirstName = t.FirstName,
						PatronymicName = t.PatronymicName,
						LastName = t.LastName,
						BirthDate = t.BirthDate
					}).ToList()
				});
			
		}
	}
}