﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TvMazeScraperTest.Repositories.Entities;
using TvMazeScraperTest.Repositories.Enums;

namespace TvMazeScraperTest.Repositories.Configurations
{
	/// <summary>
	/// ParticipantConfiguration
	/// </summary>
	/// <seealso cref="Microsoft.EntityFrameworkCore.IEntityTypeConfiguration{TvMazeScraperTest.Repositories.Entities.ParticipantEntity}" />
	public class ParticipantConfiguration : IEntityTypeConfiguration<ParticipantEntity>
	{
		/// <summary>
		/// Configures the entity of type <typeparamref name="TEntity" />.
		/// </summary>
		/// <param name="builder">The builder to be used to configure the entity type.</param>
		public void Configure(EntityTypeBuilder<ParticipantEntity> builder)
		{
			//builder.ToTable("Participants");

			builder.Property(p => p.Id)
				.HasColumnType("int");

			builder.HasKey(p => p.Id);

			builder.Property(p => p.FirstName)
				.HasColumnType("nvarchar(255)")
				.IsRequired();

			builder.Property(p => p.PatronymicName)
				.HasColumnType("nvarchar(255)")
				.IsRequired(false);
				
			builder.Property(p => p.LastName)
				.HasColumnType("nvarchar(255)")
				.IsRequired();

			builder.Property(p => p.BirthDate)
				.HasColumnType("date")
				.IsRequired();

			builder.HasMany(p => p.ShowParticipants)
				.WithOne(s => s.ParticipantEntity)
				.HasForeignKey(s => s.ParticipantId);

			builder.Property(p =>p.Role)
				.HasColumnType("int")
				.HasConversion(o =>(int) o,
				i => (ParticipantRole) i);

			builder.Property(p => p.Sex)
				.HasColumnType("nvarchar(16)")
				.HasConversion(o => o.ToString(),
					i => (ParticipantSex)Enum.Parse(typeof(ParticipantSex), i));
			
			builder.HasData(
				new ParticipantEntity()
				{
					Id = 1,
					FirstName = "Sean",
					LastName = "Bean",
					BirthDate = new DateTime(1959, 4, 17),
					Role = ParticipantRole.Actor,
					Sex = ParticipantSex.Male
				},
				new ParticipantEntity()
				{
					Id = 2,
					FirstName = "Peter",
					PatronymicName = "Hayden",
					LastName = "Dinklage",
					BirthDate = new DateTime(1969, 6, 11),
					Role = ParticipantRole.Actor,
					Sex = ParticipantSex.Male
				},
				new ParticipantEntity()
				{
					Id = 3,
					FirstName = "Matthew",
					PatronymicName = "David",
					LastName = "McConaughey",
					BirthDate = new DateTime(1969, 10, 4),
					Role = ParticipantRole.Actor,
					Sex = ParticipantSex.Male
				},
				new ParticipantEntity()
				{
					Id = 4,
					FirstName = "Woody",
					LastName = "Harrelson",
					BirthDate = new DateTime(1961, 7, 23),
					Role = ParticipantRole.Actor,
					Sex = ParticipantSex.Male
				},
				new ParticipantEntity()
				{
					Id = 5,
					FirstName = "Emilia",
					LastName = "Clarke",
					BirthDate = new DateTime(1986, 10, 23),
					Role = ParticipantRole.Actor,
					Sex = ParticipantSex.Female
				}

			);
		}
	}
}