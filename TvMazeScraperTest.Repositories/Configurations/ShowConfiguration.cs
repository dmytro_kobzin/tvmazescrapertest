﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TvMazeScraperTest.Repositories.Entities;

namespace TvMazeScraperTest.Repositories.Configurations
{
	/// <summary>
	/// ShowConfiguration 
	/// </summary>
	/// <seealso cref="Microsoft.EntityFrameworkCore.IEntityTypeConfiguration{TvMazeScraperTest.Repositories.Entities.ShowEntity}" />
	public class ShowConfiguration : IEntityTypeConfiguration<ShowEntity>
	{
		/// <summary>
		/// Configures the entity of type <typeparamref name="TEntity" />.
		/// </summary>
		/// <param name="builder">The builder to be used to configure the entity type.</param>
		public void Configure(EntityTypeBuilder<ShowEntity> builder)
		{
			//builder.ToTable("Showes");

			builder.Property(p => p.Id)
				.HasColumnType("int");

			builder.HasKey(p => p.Id);

			builder.Property(p => p.Name)
				.HasColumnType("nvarchar(255)")
				.IsRequired();

			builder.Property(p => p.Started)
				.HasColumnType("date")
				.IsRequired();
				
			builder.Property(p => p.Finished)
				.HasColumnType("date")
				.IsRequired(false);

			builder.Property(p => p.Seasons)
				.HasColumnType("int")
				.IsRequired()
				.HasDefaultValue(1);

			builder.HasMany(p => p.ShowParticipants)
				.WithOne(s => s.ShowEntity)
				.HasForeignKey(s => s.ShowId);

			builder.HasData(
			 	new ShowEntity() { Id = 1, Name = "Games of Thrones", Started = new DateTime(2011, 4, 17), Seasons = 8},
			 	new ShowEntity() { Id = 2, Name = "True Detective", Started = new DateTime(2014, 1, 12), Seasons = 2}
			);
		}
	}
}