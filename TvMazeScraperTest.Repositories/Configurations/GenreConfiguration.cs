﻿using System;
using System.Globalization;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TvMazeScraperTest.Repositories.Entities;
using TvMazeScraperTest.Repositories.Enums;

namespace TvMazeScraperTest.Repositories.Configurations
{
	/// <summary>
	/// GenreConfiguration
	/// </summary>
	/// <seealso cref="Microsoft.EntityFrameworkCore.IEntityTypeConfiguration{TvMazeScraperTest.Repositories.Entities.GenreEntity}" />
	public class GenreConfiguration : IEntityTypeConfiguration<GenreEntity>
	{
		/// <summary>
		/// Configures the entity of type <typeparamref name="TEntity" />.
		/// </summary>
		/// <param name="builder">The builder to be used to configure the entity type.</param>
		public void Configure(EntityTypeBuilder<GenreEntity> builder)
		{
			//builder.ToTable("Genres");

			builder.Property(p => p.Id)
				.HasColumnType("int");

			builder.HasKey(p => p.Id);
			
			builder.Property(p =>p.ShowGenre)
				.HasColumnType("nvarchar(50)")
				.HasConversion(o => o.ToString(),
				i => (ShowGenre) Enum.Parse(typeof(ShowGenre), i));

			builder.HasOne<ShowEntity>(s => s.Show)
				.WithMany(g => g.Genres)
				.HasForeignKey(k => k.ShowId);

				builder.HasData(
				new GenreEntity()
				{
					Id = 1,
					ShowId = 1,
					ShowGenre = ShowGenre.Fantasy
				},new GenreEntity()
				{
					Id = 2,
					ShowId = 2,
					ShowGenre = ShowGenre.Detective
				}
			);
		}
	}
}