﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TvMazeScraperTest.Repositories.Entities;

namespace TvMazeScraperTest.Repositories.Configurations
{
	/// <summary>
	/// 
	/// </summary>
	/// <seealso cref="Microsoft.EntityFrameworkCore.IEntityTypeConfiguration{TvMazeScraperTest.Repositories.Entities.ShowParticipantEntity}" />
	public class ShowParticipantConfiguration : IEntityTypeConfiguration<ShowParticipantEntity>
	{
		/// <summary>
		/// Configures the entity of type <typeparamref name="TEntity" />.
		/// </summary>
		/// <param name="builder">The builder to be used to configure the entity type.</param>
		public void Configure(EntityTypeBuilder<ShowParticipantEntity> builder)
		{
			//builder.ToTable("ShowParticipants");

			builder.HasKey(p => new {p.ShowId, p.ParticipantId });

			builder.HasAlternateKey(p => p.Id);

			builder.HasOne(p => p.ShowEntity)
				.WithMany(o => o.ShowParticipants)
				.HasForeignKey(f => f.ShowId);

			builder.HasOne(p => p.ParticipantEntity)
				.WithMany(o => o.ShowParticipants)
				.HasForeignKey(f => f.ParticipantId);

			//builder.HasData(
			//	new ShowParticipantEntity() { ParticipantId = 1, ShowId = 1},
			//	new ShowParticipantEntity() { ParticipantId = 2, ShowId = 1},
			//	new ShowParticipantEntity() { ParticipantId = 3, ShowId = 2},
			//	new ShowParticipantEntity() { ParticipantId = 4, ShowId = 2}
			//	);
		}
	}
}