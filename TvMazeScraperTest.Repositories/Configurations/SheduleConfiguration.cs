﻿using System;
using System.Globalization;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NodaTime;
using TvMazeScraperTest.Repositories.Entities;
using TvMazeScraperTest.Repositories.Enums;

namespace TvMazeScraperTest.Repositories.Configurations
{
	/// <summary>
	/// SheduleConfiguration
	/// </summary>
	/// <seealso cref="Microsoft.EntityFrameworkCore.IEntityTypeConfiguration{TvMazeScraperTest.Repositories.Entities.SheduleEntity}" />
	public class SheduleConfiguration : IEntityTypeConfiguration<SheduleEntity>
	{
		/// <summary>
		/// Configures the entity of type <typeparamref name="TEntity" />.
		/// </summary>
		/// <param name="builder">The builder to be used to configure the entity type.</param>
		public void Configure(EntityTypeBuilder<SheduleEntity> builder)
		{
			builder.ToTable("Shedules");

			builder.Property(p => p.Id)
				.HasColumnType("int");

			builder.HasKey(p => p.Id);
			
			builder.Property(p =>p.Time)
				.HasColumnType("bigint")
				.HasConversion(o => o.Ticks,
				i => new TimeSpan(i));

			builder.Property(p => p.DayOfWeek)
				.HasColumnType("nvarchar(16)")
				.HasConversion(o => o.ToString(),
					i => (DayOfWeek) Enum.Parse(typeof(DayOfWeek), i));

			builder.HasOne<ShowEntity>(s => s.Show)
				.WithMany(g => g.Shedules)
				.HasForeignKey(k => k.ShowId);

				builder.HasData(
				new SheduleEntity()
				{
					Id = 1,
					ShowId = 1,
					Time = new TimeSpan(0,20,0,0),
					DayOfWeek = DayOfWeek.Sunday
				},
				new SheduleEntity()
				{
					Id = 2,
					ShowId = 2,
					Time = new TimeSpan(0,21,0,0),
					DayOfWeek = DayOfWeek.Sunday
				}
			);
		}
	}
}