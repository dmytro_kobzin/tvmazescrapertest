﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using TvMazeScraperTest.Common;
using TvMazeScraperTest.Repositories.Entities;
using TvMazeScraperTest.Repositories.Infrastructure.EF;

namespace TvMazeScraperTest.Repositories.Repositories
{
	/// <summary>
	/// ShowEntity repository.
	/// </summary>
	/// <seealso cref="TvMazeScraperTest.Repositories.Repositories.IShowRepository" />
	public class ShowRepository : IShowRepository
	{
		/// <summary>
		/// Gets or sets the context.
		/// </summary>
		/// <value>
		/// The context.
		/// </value>
		public TestDataBaseContext Context { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="ShowRepository"/> class.
		/// </summary>
		[Obsolete("For Unit testing and moq")]
		public ShowRepository()
		{
			this.Context = new TestContextFactory().CreateDbContext(new string []{});
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ShowRepository"/> class.
		/// </summary>
		/// <param name="context">The context.</param>
		public ShowRepository(TestDataBaseContext context)
		{
			this.Context = context;
		}

		/// <summary>
		/// Adds the specified source.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <returns></returns>
		public ShowEntity Add(ShowEntity source)
		{
			try
			{
				using (var context = this.Context)
				{
					this.Context.Showes.Add(source);
					this.Context.SaveChanges();
				}
				return source;
			}
			catch (Exception e)
			{
				ApplicationLogger.Logger.Fatal(e.ToString());
				throw;
			}
		}

		/// <summary>
		/// Gets the specified identifier.
		/// </summary>
		/// <param name="id">The identifier.</param>
		/// <returns></returns>
		public ShowEntity Get(int id)
		{
			try
			{
				using (var context = this.Context)
				{
					var show = this.Context.Showes
						.Include(e => e.Genres)
						.Include(e => e.Shedules)
						.Include(t => t.ShowParticipants)
						.ThenInclude(participant => participant.ParticipantEntity)
						.First(p => p.Id == id);

					return show;
				}
			}
			catch (Exception e)
			{
				ApplicationLogger.Logger.Fatal(e.ToString());
				throw;
			}
		}

		/// <summary>
		/// Adds the or update.
		/// </summary>
		/// <param name="showEntity">The showEntity.</param>
		public void AddOrUpdate(ShowEntity showEntity)
		{
			try
			{
				using (var context = this.Context)
				{
					var source = this.Context.Showes.First(p => p.Id == showEntity.Id);
					if (source != null)
					{
						var id = source.Id;
						source = showEntity;
						source.Id = id;
					}
					else
					{
						this.Add(showEntity);
					}

					this.Context.SaveChanges();
				}
			}
			catch (Exception e)
			{
				ApplicationLogger.Logger.Fatal(e.ToString());
				throw;
			}
		}

		/// <summary>
		/// Deletes the specified identifier.
		/// </summary>
		/// <param name="id">The identifier.</param>
		public void Delete(int id)
		{
			try
			{
				using (var context = this.Context)
				{
					var source = this.Context.Showes.First(p => p.Id == id);
					if (source != null)
					{
						this.Context.Showes.Remove(source);
					}

					this.Context.SaveChanges();
				}
			}
			catch (Exception e)
			{
				ApplicationLogger.Logger.Fatal(e.ToString());
				throw;
			}
		}

		/// <summary>
		/// Saves the specified entity.
		/// </summary>
		/// <param name="entity">The entity.</param>
		public void Save(ShowEntity entity)
		{
			this.AddOrUpdate(entity);
		}

		/// <summary>
		/// Gets the entities.
		/// </summary>
		/// <param name="query">The query.</param>
		/// <returns></returns>
		/// <exception cref="System.NotImplementedException"></exception>
		public ICollection<ShowEntity> GetEntities(Expression<Func<ShowEntity, bool>> query)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Searches the specified name.
		/// </summary>
		/// <param name="search">The search.</param>
		/// <returns></returns>
		/// <exception cref="System.NotImplementedException"></exception>
		public ICollection<ShowEntity> Search(Expression<Func<ShowEntity, bool>> search)
		{
			try
			{
				using (var context = this.Context)
				{
					var entities = this.Context.Showes
						.Include(e => e.Genres)
						.Include(e => e.Shedules)
						.Include(t => t.ShowParticipants)
						.ThenInclude(participant => participant.ParticipantEntity)
						.Where(search)
						.ToList();

					return entities;
				}
			}
			catch (Exception e)
			{
				ApplicationLogger.Logger.Fatal(e.ToString());
				throw;
			}
		}
	}
}