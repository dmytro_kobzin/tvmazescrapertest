﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using TvMazeScraperTest.Repositories.Entities;

namespace TvMazeScraperTest.Repositories.Repositories
{
	/// <summary>
	/// Interface of showEntity repository.
	/// </summary>
	/// <seealso cref="TvMazeScraperTest.Repositories.Repositories.IRepository{TvMazeScraperTest.Repositories.Entities.Show}" />
	public interface IShowRepository : IRepository<ShowEntity>
	{
		/// <summary>
		/// Searches the specified name.
		/// </summary>
		/// <param name="search">The search.</param>
		/// <returns></returns>
		ICollection<ShowEntity> Search(Expression<Func<ShowEntity, bool>> search);
	}
}