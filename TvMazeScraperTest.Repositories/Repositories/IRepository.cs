﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using TvMazeScraperTest.Repositories.Entities;
using TvMazeScraperTest.Repositories.Infrastructure.EF;

namespace TvMazeScraperTest.Repositories.Repositories
{
	/// <summary>
	/// Base interface
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public interface IRepository<T> where T : BaseEntity
	{
		/// <summary>
		/// Gets or sets the context.
		/// </summary>
		/// <value>
		/// The context.
		/// </value>
		TestDataBaseContext Context { get; set; }

		/// <summary>
		/// Adds the specified source.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <returns></returns>
		T Add(T source);

		/// <summary>
		/// Gets the specified identifier.
		/// </summary>
		/// <param name="id">The identifier.</param>
		/// <returns></returns>
		T Get(int id);

		/// <summary>
		/// Updates the specified source.
		/// </summary>
		/// <param name="source">The source.</param>
		void AddOrUpdate(T source);

		/// <summary>
		/// Deletes the specified identifier.
		/// </summary>
		/// <param name="id">The identifier.</param>
		void Delete(int id);

		/// <summary>
		/// Saves this instance.
		/// </summary>
		/// <param name="entity">The entity.</param>
		void Save(T entity);

		/// <summary>
		/// Gets the entities.
		/// </summary>
		/// <param name="query">The query.</param>
		/// <returns></returns>
		ICollection<T> GetEntities(Expression<Func<T, bool>> query);
	}
}