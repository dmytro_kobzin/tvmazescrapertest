﻿using System.ComponentModel;

namespace TvMazeScraperTest.Repositories.Enums
{
	/// <summary>
	/// Participant Sex
	/// </summary>
	public enum ParticipantSex
	{
		/// <summary>
		/// The male
		/// </summary>
		[Description("Male")]
		Male = 1,

		/// <summary>
		/// The female
		/// </summary>
		[Description("Female")]
		Female = 2
	}
}