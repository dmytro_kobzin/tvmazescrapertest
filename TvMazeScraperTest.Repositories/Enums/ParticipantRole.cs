﻿using System.ComponentModel;

namespace TvMazeScraperTest.Repositories.Enums
{
	/// <summary>
	/// Participant Role
	/// </summary>
	public enum ParticipantRole
	{
		/// <summary>
		/// The actor
		/// </summary>
		[Description("Actor")]
		Actor = 1,

		/// <summary>
		/// The director
		/// </summary>
		[Description("Director")]
		Director = 2,

		/// <summary>
		/// The cameraman
		/// </summary>
		[Description("Cameraman")]
		Cameraman = 3,

		/// <summary>
		/// The producer
		/// </summary>
		[Description("Producer")]
		Producer = 4
	}
}