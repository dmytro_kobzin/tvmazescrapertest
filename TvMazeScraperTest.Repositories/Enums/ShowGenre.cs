﻿using System.ComponentModel;

namespace TvMazeScraperTest.Repositories.Enums
{
	/// <summary>
	/// Show's genre
	/// </summary>
	public enum ShowGenre
	{
		/// <summary>
		/// The detective
		/// </summary>
		[Description("Detective")]
		Detective = 1,

		/// <summary>
		/// The fantasy
		/// </summary>
		[Description("Fantasy")]
		Fantasy = 2
	}
}