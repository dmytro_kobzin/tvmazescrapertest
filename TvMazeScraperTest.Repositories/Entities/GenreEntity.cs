﻿using TvMazeScraperTest.Repositories.Enums;

namespace TvMazeScraperTest.Repositories.Entities
{
	/// <summary>
	/// Genre
	/// </summary>
	/// <seealso cref="TvMazeScraperTest.Repositories.Entities.BaseEntity" />
	public class GenreEntity : BaseEntity
	{
		/// <summary>
		/// Gets or sets the show identifier.
		/// </summary>
		/// <value>
		/// The show identifier.
		/// </value>
		public virtual int ShowId { get; set; }

		/// <summary>
		/// Gets or sets the show.
		/// </summary>
		/// <value>
		/// The show.
		/// </value>
		public virtual ShowEntity Show { get; set; }

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		public virtual ShowGenre ShowGenre { get; set; }
	}
}