﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TvMazeScraperTest.Repositories.Entities
{
	/// <summary>
	/// ShowEntity-participant relations.
	/// </summary>
	/// <seealso cref="TvMazeScraperTest.Repositories.Entities.IEntity" />
	public class ShowParticipantEntity : BaseEntity
    {
		
		/// <summary>
		/// Gets or sets the showEntity identifier.
		/// </summary>
		/// <value>
		/// The showEntity identifier.
		/// </value>
		public virtual int ShowId { get; set; }

		/// <summary>
		/// Gets or sets the showEntity.
		/// </summary>
		/// <value>
		/// The show.
		/// </value>
		public virtual ShowEntity ShowEntity { get; set; }

		/// <summary>
		/// Gets or sets the participant identifier.
		/// </summary>
		/// <value>
		/// The participant identifier.
		/// </value>
		public virtual int ParticipantId { get; set; }

		/// <summary>
		/// Gets or sets the participant.
		/// </summary>
		/// <value>
		/// The participant.
		/// </value>
		public virtual ParticipantEntity ParticipantEntity { get; set; }
	}
}
