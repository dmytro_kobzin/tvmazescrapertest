﻿using System;
using NodaTime;

namespace TvMazeScraperTest.Repositories.Entities
{
	/// <summary>
	/// SheduleEntity
	/// </summary>
	/// <seealso cref="TvMazeScraperTest.Repositories.Entities.BaseEntity" />
	public class SheduleEntity : BaseEntity
	{
		/// <summary>
		/// Gets or sets the show identifier.
		/// </summary>
		/// <value>
		/// The show identifier.
		/// </value>
		public virtual int ShowId { get; set; }

		/// <summary>
		/// Gets or sets the show.
		/// </summary>
		/// <value>
		/// The show.
		/// </value>
		public virtual ShowEntity Show { get; set; }

		/// <summary>
		/// Gets or sets the time.
		/// </summary>
		/// <value>
		/// The time.
		/// </value>
		public virtual TimeSpan Time { get; set; }

		/// <summary>
		/// Gets or sets the day of week.
		/// </summary>
		/// <value>
		/// The day of week.
		/// </value>
		public virtual DayOfWeek DayOfWeek { get; set; }
	}
}