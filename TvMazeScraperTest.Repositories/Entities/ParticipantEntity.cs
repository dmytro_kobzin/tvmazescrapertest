﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using TvMazeScraperTest.Repositories.Enums;

namespace TvMazeScraperTest.Repositories.Entities
{
	/// <summary>
	/// ParticipantEntity
	/// </summary>
	/// <seealso cref="TvMazeScraperTest.Repositories.Entities.IEntity" />
	public class ParticipantEntity : BaseEntity
    {
	    public ParticipantEntity()
	    {
		    this.ShowParticipants = new List<ShowParticipantEntity>();
	    }

	    /// <summary>
		/// Gets or sets the first name.
		/// </summary>
		/// <value>
		/// The first name.
		/// </value>
		public virtual string FirstName { get; set; }

		/// <summary>
		/// Gets or sets the name of the patronymic.
		/// </summary>
		/// <value>
		/// The name of the patronymic.
		/// </value>
		public virtual string PatronymicName { get; set; }

		/// <summary>
		/// Gets or sets the last name.
		/// </summary>
		/// <value>
		/// The last name.
		/// </value>
		public virtual string LastName { get; set; }

		/// <summary>
		/// Gets or sets the birth date.
		/// </summary>
		/// <value>
		/// The birth date.
		/// </value>
		public virtual DateTime BirthDate { get; set; }

		/// <summary>
		/// Gets or sets the role.
		/// </summary>
		/// <value>
		/// The role.
		/// </value>
		public virtual ParticipantRole Role { get; set; }

		/// <summary>
		/// Gets or sets the showEntity participants.
		/// </summary>
		/// <value>
		/// The showEntity participants.
		/// </value>
		public virtual ICollection<ShowParticipantEntity> ShowParticipants { get; set; }

		/// <summary>
		/// Gets or sets the sex.
		/// </summary>
		/// <value>
		/// The sex.
		/// </value>
		public virtual ParticipantSex Sex { get; set; }
    }
}
