﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace TvMazeScraperTest.Repositories.Entities
{
	/// <summary>
	/// ShowEntity.
	/// </summary>
	/// <seealso cref="TvMazeScraperTest.Repositories.Entities.BaseEntity" />
	public class ShowEntity : BaseEntity
    {
	    public ShowEntity()
	    {
			this.ShowParticipants = new List<ShowParticipantEntity>();
	    }

	    /// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		public virtual string Name { get; set; }

		/// <summary>
		/// Gets or sets the showEntity participants.
		/// </summary>
		/// <value>
		/// The showEntity participants.
		/// </value>
		public virtual ICollection<ShowParticipantEntity> ShowParticipants { get; set; }
		
		/// <summary>
		/// Gets or sets the started.
		/// </summary>
		/// <value>
		/// The started.
		/// </value>
		public virtual DateTime Started { get; set; }

		/// <summary>
		/// Gets or sets the finished.
		/// </summary>
		/// <value>
		/// The finished.
		/// </value>
		public virtual DateTime? Finished { get; set; }

		/// <summary>
		/// Gets or sets the seasons.
		/// </summary>
		/// <value>
		/// The seasons.
		/// </value>
		public virtual int Seasons { get; set; }

		/// <summary>
		/// Gets or sets the genres.
		/// </summary>
		/// <value>
		/// The genres.
		/// </value>
		public virtual ICollection<GenreEntity> Genres { get; set; }

		/// <summary>
		/// Gets or sets the shedules.
		/// </summary>
		/// <value>
		/// The shedules.
		/// </value>
		public virtual ICollection<SheduleEntity> Shedules { get; set; }
	    
    }
}
