﻿namespace TvMazeScraperTest.Repositories.Entities
{
	/// <summary>
	/// Base eintity.
	/// </summary>
	/// <seealso cref="TvMazeScraperTest.Repositories.Entities.IEntity" />
	public abstract class BaseEntity : IEntity
	{
		/// <summary>
		/// Gets or sets the identifier.
		/// </summary>
		/// <value>
		/// The identifier.
		/// </value>
		public virtual int Id { get; set; }
	}
}