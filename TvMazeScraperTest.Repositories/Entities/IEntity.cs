﻿namespace TvMazeScraperTest.Repositories.Entities
{
	/// <summary>
	/// Entity's interface
	/// </summary>
	public interface IEntity
	{
		/// <summary>
		/// Gets or sets the identifier.
		/// </summary>
		/// <value>
		/// The identifier.
		/// </value>
		int Id { get; set; }
	}
}