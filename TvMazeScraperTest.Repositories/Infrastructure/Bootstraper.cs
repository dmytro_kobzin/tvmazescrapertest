﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using TvMazeScraperTest.Repositories.Entities;
using TvMazeScraperTest.Repositories.Infrastructure.EF;
using TvMazeScraperTest.Repositories.Repositories;

namespace TvMazeScraperTest.Repositories
{
	/// <summary>
	/// Bootstrapper
	/// </summary>
	public static class Bootstraper
	{
		/// <summary>
		/// Registers the specified services.
		/// </summary>
		/// <param name="services">The services.</param>
		public static void Register(IServiceCollection services)
		{
			services.AddDbContext<TestDataBaseContext>(options =>
				options.UseSqlServer(@"Server=(localdb)\MSSQLLocalDB;Database=TvMazeScraperTestDB;Trusted_Connection=True;",
					assembly => assembly.MigrationsAssembly("TvMazeScraperTest.Repositories")));

			services.AddTransient<IShowRepository, ShowRepository>();
		}

		/// <summary>
		/// Registers the mappings.
		/// </summary>
		public static void RegisterMappings()
		{
		}
	}
}