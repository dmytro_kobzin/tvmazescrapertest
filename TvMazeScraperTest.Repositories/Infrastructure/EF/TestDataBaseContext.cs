﻿using Microsoft.EntityFrameworkCore;
using TvMazeScraperTest.Repositories.Configurations;
using TvMazeScraperTest.Repositories.Entities;

namespace TvMazeScraperTest.Repositories.Infrastructure.EF
{
	/// <summary>
	/// DataBase conext
	/// </summary>
	/// <seealso cref="Microsoft.EntityFrameworkCore.DbContext" />
	public class TestDataBaseContext : DbContext
    {
		#region Properties

		/// <summary>
		/// Gets or sets the showes.
		/// </summary>
		/// <value>
		/// The showes.
		/// </value>
		public virtual DbSet<ShowEntity> Showes { get; set; }

		/// <summary>
		/// Gets or sets the participants.
		/// </summary>
		/// <value>
		/// The participants.
		/// </value>
		public virtual DbSet<ParticipantEntity> Participants { get; set; }

		/// <summary>
		/// Gets or sets the showEntity participants.
		/// </summary>
		/// <value>
		/// The showEntity participants.
		/// </value>
		public virtual DbSet<ShowParticipantEntity> ShowParticipants { get; set; }

		/// <summary>
		/// Gets or sets the genres.
		/// </summary>
		/// <value>
		/// The genres.
		/// </value>
		public virtual DbSet<GenreEntity> Genres { get; set; }

		/// <summary>
		/// Gets or sets the shedule entities.
		/// </summary>
		/// <value>
		/// The shedule entities.
		/// </value>
		public virtual DbSet<SheduleEntity> SheduleEntities{ get; set; }


		#endregion

		#region region Ctor

		/// <summary>
		/// Initializes a new instance of the <see cref="TestDataBaseContext"/> class.
		/// </summary>
		protected TestDataBaseContext()
	    {
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="TestDataBaseContext"/> class.
		/// </summary>
		/// <param name="options">The options.</param>
		public TestDataBaseContext(DbContextOptions<TestDataBaseContext> options) : base(options)
	    {
	    }

		#endregion

		#region Members

		/// <summary>
		/// <para>
		/// Override this method to configure the database (and other options) to be used for this context.
		/// This method is called for each instance of the context that is created.
		/// The base implementation does nothing.
		/// </para>
		/// <para>
		/// In situations where an instance of <see cref="T:Microsoft.EntityFrameworkCore.DbContextOptions" /> may or may not have been passed
		/// to the constructor, you can use <see cref="P:Microsoft.EntityFrameworkCore.DbContextOptionsBuilder.IsConfigured" /> to determine if
		/// the options have already been set, and skip some or all of the logic in
		/// <see cref="M:Microsoft.EntityFrameworkCore.DbContext.OnConfiguring(Microsoft.EntityFrameworkCore.DbContextOptionsBuilder)" />.
		/// </para>
		/// </summary>
		/// <param name="optionsBuilder">A builder used to create or modify options for this context. Databases (and other extensions)
		/// typically define extension methods on this object that allow you to configure the context.</param>
		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
	    {
			base.OnConfiguring(optionsBuilder);
	    }

		/// <summary>
		/// Override this method to further configure the model that was discovered by convention from the entity types
		/// exposed in <see cref="T:Microsoft.EntityFrameworkCore.DbSet`1" /> properties on your derived context. The resulting model may be cached
		/// and re-used for subsequent instances of your derived context.
		/// </summary>
		/// <param name="modelBuilder">The builder being used to construct the model for this context. Databases (and other extensions) typically
		/// define extension methods on this object that allow you to configure aspects of the model that are specific
		/// to a given database.</param>
		/// <remarks>
		/// If a model is explicitly set on the options for this context (via <see cref="M:Microsoft.EntityFrameworkCore.DbContextOptionsBuilder.UseModel(Microsoft.EntityFrameworkCore.Metadata.IModel)" />)
		/// then this method will not be run.
		/// </remarks>
		protected override void OnModelCreating(ModelBuilder modelBuilder)
	    {
		    modelBuilder.ApplyConfiguration(new ShowConfiguration());
		    modelBuilder.ApplyConfiguration(new ParticipantConfiguration());
		    //modelBuilder.ApplyConfiguration(new ShowParticipantConfiguration());
			
			base.OnModelCreating(modelBuilder);
	    }

	    #endregion
	}
}
