﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using TvMazeScraperTest.Repositories.Infrastructure.EF;

namespace TvMazeScraperTest.Repositories.Infrastructure.EF
{
	public class TestContextFactory : IDesignTimeDbContextFactory<TestDataBaseContext>
	{
		public TestDataBaseContext CreateDbContext(string[] args)
		{
			var optionsBuilder = new DbContextOptionsBuilder<TestDataBaseContext>();
			optionsBuilder.UseSqlServer(@"Server=(localdb)\MSSQLLocalDB;Database=TvMazeScraperTestDB;Trusted_Connection=True;");
			optionsBuilder.EnableSensitiveDataLogging();
			return new TestDataBaseContext(optionsBuilder.Options);
		}
	}
}